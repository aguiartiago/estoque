package br.com.ufscar.dao;

import br.com.ufscar.entity.Department;

public class DepartmentDAO extends GenericDAO<Department>{

	private static final long serialVersionUID = 1L;

	public DepartmentDAO() {
		super(Department.class);
	}


}
