package br.com.ufscar.dao.common;

public interface NativeSQL {

	String byName(String queryName);

}